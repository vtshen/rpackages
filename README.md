# Announcement

The R package SeqADMM is not available to the public at this moment.

# R packages

This repo contains the packages that were built during the PhD study and were available in public. The detailed source codes are available at https://bitbucket.org/%7B95ca0640-689e-4e18-855e-15c18abd7733%7D/

2. The package AHM_1.0.1.tar.gz (available in CRAN https://cran.r-project.org/web/packages/AHM/index.html) is for the paper:

- Shen S., Kang, L., and Deng, X. 2019. Additive Heredity Model for the Analysis of Mixture-of-Mixtures Experiments. Technometrics, in press.

1. The package TOM_1.1.0.tar.gz is for the papers:

- Shen S., Mao, H., and Deng, X. 2019. Rejoinder to 'Response to Open Challenges on Correlation of Intermediate and Final Measurements - Solving the Impossible Problem? Quality Engineering, 31(3), 516-521.
- Shen S., Mao, H., and Deng, X. 2019. An EM-Algorithm Approach to Open Challenges on Correlation of Intermediate and Final Measurements. Quality Engineering, 31(3), 505-510.
 

# Notes

- The use of R code to reproduce the simulations in the paper Response to "Responses to Open Challenges on Correlation of Intermediate and Final Measurements - Solving the Impossible Problem?" is available in the vignette of package TOM.
 
- Typo in the paper An EM-Algorithm Approach to Open Challenges on Correlation of Intermediate and Final Measurements. (2018)

In the simulation section, the settings, $\beta_0$ and $\alpha$, are not correctly typed. They should be $\beta_0$ = 30 and $\alpha$ = 5, instead of $\beta_0$ = 3 and $\alpha$ = 1.

(Last update: July 28, 2019)

